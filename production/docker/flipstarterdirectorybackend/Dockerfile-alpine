FROM python:3.8-alpine

COPY ./requirements.txt /

RUN echo $(cat /etc/*release*)
# we need this massive command if we split it into different commands
# that's different layers and the image grows substantially
RUN apk update && \
    apk add --no-cache --virtual build-deps gcc musl-dev jpeg-dev zlib-dev libffi-dev libressl-dev rust cargo && \
    apk add --no-cache postgresql-dev && \
    python3.8 -m pip install --no-cache-dir -r requirements.txt && \
    python3.8 -m pip install --no-cache-dir daphne && \
    apk del build-deps && \
    apk del postgresql-dev
RUN apk add --no-cache libpq jpeg zlib libffi libressl musl

COPY ./production/docker/flipstarterdirectorybackend/cron_base_scripts/flipstarter_base /cron_base_scripts/flipstarter_base
COPY ./production/docker/flipstarterdirectorybackend/cron_scripts /etc/periodic/daily/
COPY ./production/docker/flipstarterdirectorybackend/cron_scripts /etc/periodic/weekly/

RUN apk add bash
COPY ./flipstarterdirectorybackend /flipstarterdirectorybackend

COPY ./production/docker/flipstarterdirectorybackend/start-django.sh /start.sh

RUN adduser -s /bin/bash -u 2000 -D flip
RUN chown -R flip:flip /flipstarterdirectorybackend
USER flip

ENV DJANGO_SETTINGS_MODULE=flipstarterdirectorybackend.prod_settings
ENV PYTHONPATH=/flipstarterdirectorybackend
ENV ALPINE=yes

CMD ["bash", "/start.sh"]

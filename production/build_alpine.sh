VERSION=$1
SUDO=$2

$SUDO docker build -t flipstarterdirectorybackend:${VERSION}-alpine -f docker/flipstarterdirectorybackend/Dockerfile-alpine ..

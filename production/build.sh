VERSION=$1
SUDO=$2

pushd .
cd ../flipstarterdirectorybackend/
DJANGO_SETTINGS_MODULE=flipstarterdirectorybackend.prod_settings python3 manage.py collectstatic
popd

echo
echo
echo
$SUDO docker build -t flipstarterdirectory-nginx:${VERSION} -f docker/nginx/Dockerfile ..

echo
echo
echo
$SUDO docker build -t chrome:${VERSION} -f docker/chrome/Dockerfile ..

echo
echo
echo
$SUDO docker build -t flipstarterdirectorybackend:${VERSION} -f docker/flipstarterdirectorybackend/Dockerfile ..



# Flipstarter Directory API

## Setup

Prefer python3.8+
```
python3 -m env .
```

```
pip install -r requirements.txt
```

## Run

```
cd flipstarterdirectorybackend
python manage.py runserver
```

Open the browser and go to
```
http://127.0.0.1:8000
```

## Build and deploy

See production directory.


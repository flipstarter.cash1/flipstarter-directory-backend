from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from django.utils.deconstruct import deconstructible


@deconstructible
class RegexOrEmptyValidator(RegexValidator):

    def __call__(self, value: str) -> None:
        # if the value is empty string skip regex
        if value == '':
            return
        return super().__call__(value)
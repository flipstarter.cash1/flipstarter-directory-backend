from django.apps import AppConfig


class FlipstarterdirectoryConfig(AppConfig):
    name = 'flipstarterdirectory'

from django.db import connection

# timeout that we wait for the website to load (withoout the ajax requests)
WEBSITE_TIMEOUT = 10
# timeout that we wait for ajax requests
AJAX_TIMEOUT = 10

def is_postgresql_or_mariadb():
    connection_vendor = connection.vendor.lower()
    return connection_vendor == 'postgresql' or connection_vendor == 'mariadb'

def is_sqlite():
    connection_vendor = connection.vendor.lower()
    return connection_vendor == 'sqlite'

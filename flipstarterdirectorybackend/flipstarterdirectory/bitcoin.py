SATOSHIS_PER_BCH = 100000000

def bch_to_satoshi(bch):
    return bch * SATOSHIS_PER_BCH

def satoshi_to_bch(satoshi):
    return satoshi / SATOSHIS_PER_BCH
from django.urls import include, re_path

from rest_framework import routers

from . import views


app_name = 'flipstarterdirectory'

router = routers.DefaultRouter()
router.register(r'flipstarter', views.FlipstarterViewSet, basename='flipstarter')

urlpatterns = [
    re_path('', include(router.urls)),
    re_path('^flipstarter-category/$', views.FlipstarterCategoryView.as_view(), name='flipstarter-category'),
    re_path('^flipstarter-data-from-url/$', views.FlipstarterFetchDataFromURL.as_view(), name='flipstarter-data-from-url'),
]

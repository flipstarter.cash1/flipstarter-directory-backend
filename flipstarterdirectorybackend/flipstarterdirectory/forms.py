from django import forms


class FlipstarterImportJSONForm(forms.Form):
    flipstarter_json = forms.FileField(label='Flipstarter JSON File', max_length=100)

class FlipstarterImportURLForm(forms.Form):
    flipstarter_url = forms.URLField(label='Flipstarter URL', max_length=300)
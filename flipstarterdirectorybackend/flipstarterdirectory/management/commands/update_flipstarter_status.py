from django.core.management.base import BaseCommand

from flipstarterdirectory import models

from ._commandmixin import FlipstarterCommandMixin


class Command(FlipstarterCommandMixin, BaseCommand):
    help = 'Checks for flipstarter status changes where needed.'

    def handle(self, *args, **options):
        # tuple (success, failed, got_exception)
        ret = models.Flipstarter.objects.refresh_all()

        if self.nothing(ret):
            return

        self.print_success_message(
            'Successfully refreshed {count} flipstarter{plural}.',
            ret[0]
        )

        self.print_success_message(
            'Failed to refresh {count} flipstarter{plural}.',
            ret[1]
        )

        self.print_success_message(
            'Got exception{plural} for {count} flipstarter{plural}.',
            ret[2]
        )
